const {SHA256} = require("crypto-js");

const jwt = require('jsonwebtoken');

const bcrypt = require('bcryptjs')


// var message = 'I am abhinish raj';
// var hash = SHA256(message).toString();
//
// console.log(`Message: ${message}`);
// console.log(`Hash:${hash}`);
//
//
// var data ={
//     id: 4
// };
//
// var token = jwt.sign(data,'abhinish');
// console.log(token);
//
// var decode = jwt.verify(token,'abhinish');
// console.log(decode);
// //
// var token = {
//   data,
//   hash:SHA256(JSON.stringify(data)+'abhinish_secret').toString()
// };
//
// token.data.id=5;
// hash:SHA256(JSON.stringify(token.data)).toString();
//
// var resultHash = SHA256(JSON.stringify(token.data)+'abhinish_secret').toString();
//
// if(resultHash === token.hash) {
//     console.log('Data was not changed')
// }else {
//     console.log('Data was changed . Don\'t trust it')
// }

var password = 'abhinish';

//gensalt

bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
      console.log(hash);
    });
} );

var HashedPassword = '$2a$10$pmwdBqG70KJc3I0ut2poautXsSzBGd6u9W53xgEImgCdPhuOJzHMu';

bcrypt.compare(password, HashedPassword, (err, res) => {

    console.log(res);
    }

);
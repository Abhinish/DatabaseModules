const {MongoClient , ObjectID }= require('mongodb');


//object id generator
// var obj = new ObjectID();
//
// console.log(obj);

// var user = {name:'Abhinish', age:25};
// var {name} = user;
// console.log(name);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err,db) => {
    if(err){
      return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    // db.collection('Users').find().toArray().then((docs) => {
    //     console.log('Todos');
    //     console.log(JSON.stringify(docs,undefined,2));
    // },(err) => console.log('Unable to fetch todos',err)
    // );

    // db.collection('Users').find({name:'Abhinish Raj11'}).count().then((count) => {
    //     console.log(`Todos count: ${count}`);
    // }, (err) => console.log('Unable to fetch todos',err));
    //
    // db.collection('Users').find({name:'Abhinish Raj1'}).count().then((count) => {
    //     console.log(`Todos count: ${count}`);
    // }, (err) => console.log('Unable to fetch todos',err));

    db.collection('Users').findOneAndUpdate({
        name:'Abhinish Raj11'
        },{
        $set: {
            name:'Abhishek'
        },
        $inc : {
            age : 1
        }
    },{
        returnOriginal:false
    }).then(
        (result) =>
        console.log(result));
    db.close();
});
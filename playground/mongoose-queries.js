const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('../server/models/user');


// var id = '6a17f297d50929f032948a4411';

//validation for id

// if (!ObjectID.isValid(id)){
//     console.log('Id not valid');
// }

// Todo.find({
//     _id:id
// }).then((todos) => {
//     console.log('Todos', todos);
// });
//
// Todo.findOne({
//     _id:id
// }).then((todos) => {
//     console.log('Todos', todos);
// });

// Todo.findById(id).then((todo) => {
//     if(!todo){
//         return console.log('Id not found');
//     }
//    console.log('Todo by Id', todo);
// }).catch((e) => console.log(e));

//user.findById

const id = '4a169b46b6a20c9c27f1a60fjdsjflasjfl';

if (!ObjectID.isValid(id)){
    return console.log('Id not valid');
}

User.find({
    _id:id
}).then((user) =>{
   console.log(user);
});

User.findOne({
    _id:id
}).then((user) =>
    console.log(user)
);

User.findById({
    _id:id
}).then((user) => {
    if(!user){
       return console.log('User not found')
    }
    console.log('User by Id',user)})
    .catch((e) => console.log(e));



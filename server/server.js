require('./config/config');

var {ObjectID} = require('mongodb');
var express = require('express');
var bodyParser = require('body-parser');
const _ = require('lodash');
var {authenticate} = require('./middleware/authenticate');



var {mongoose} = require('./db/mongoose');
var {Todo} = require('./models/todo');
var {User} = require('./models/user');

var app = express();
const port = process.env.PORT;

//
// var Todo = mongoose.model('Todo', {
//    text : {
//        type: String,
//        required: true,
//        minlength: 1,
//        trim:true
//
//    } ,
//     complete: {
//        type:Boolean,
//         default:false
//     },
//     completedAt : {
//        type:Number,
//         default:null
//     }
// });
//
// var newTodo = new Todo({
//     text : 'Cook dinner',
//     complete:false,
//     completedAt:10
// }) ;
//
// newTodo.save().then(
//     (doc) =>
//         console.log('Saved Todo',doc)
//     ,(e) => console.log('Unable to save todo')
// );
// //Creating of todo
// var myTodo = new Todo({
//     text:'Going Home from market',
//     complete:false,
//     completedAt:20
// });
// //Saving of todo
// myTodo.save().then((doc) =>
// console.log(JSON.stringify(doc, undefined,2))
//     ,(e) =>
//         console.log('Unable to save the myTodo')
// );
//
//
// var new2Todo = new Todo({
//     text:true
// });
//
// new2Todo.save().then((doc) =>
//
//    console.log(doc)
// ,(e) => console.log('Unable to save the doc'));

//new user model email -require trim it set minlength=1
// password

// var User = mongoose.model('User', {
//     email : {
//         type: String,
//         required: true,
//         minlength: 1,
//         trim:true
//     },
//     password : {
//         type: String,
//         minlength:5,
//         trim: true
//     }
// });

//Creating new User object

// var user = new User({
//     email:'      abhinish@gmail.com        '
// });
//
// user.save().then((doc) =>
//     console.log(doc)
// ,(e)=> console.log('Unable to save the doc'));

app.use(bodyParser.json());


app.get('/',(req,res)=> {
    res
        .status(200)
        .send('You are at home');
});

app.post('/todos', authenticate, (req,res) =>{
    var todo = new Todo({
       text: req.body.text,
        completedAt: req.body.completedAt,
        complete: req.body.complete,
        _creator:req.user._id
});

    todo.save().then((doc) =>{
    res.status(200).send(doc);
    },(e) => {
        res.status(400).send(e);
    });
});


//to fetch the all the todo

app.get('/todos',authenticate,(req,res) => {
   Todo.find({
       _creator:req.user._id
   }).then((todos) => {
       res.send({todos})
   },(e) => {
       res.status(400).send(e);
   });
});

//GET /todos/12345645

app.get('/todos/:id', (req,res) => {

    console.log(req.params)

    var id = req.params.id;
        console.log(id);
    if (!ObjectID.isValid(id)){
        res
            .status(404)
            .send('Id not valid');
    }

    Todo.findById(
        id
    ).then((todos) => {
        console.log(todos);
        res.status(200).send({todos})},
        (e) => {
        res.status(400).send(e);
        }
    );

    // res.send(req.params);

    //valid id using isValid
    //404 -send empty

    //findById
    //success message
        //if todo -send it back
        //if no todo - send back 404  with empty body
    //error
        //400 and -send empty case
});

app.delete('/todos/:id',authenticate, (req,res) => {
   var id = req.param.id;

   if (!ObjectID.isValid(id)){
       return res.status(404).send();
   }

   Todo.findByIdAndRemove({
       _id:id,
       _creator: req.user._id
   }).then((todo) => {
       if(!todo) {
           return res.status(404).send();
       }

       res.send({todo});
   }).catch((e) => {
       res.status(400).send(e);
   });
});

app.patch('/todos/:id',(req,res)=>{
   var id = req.param.id;
   var body = _.pick(req.body, ['text', 'completed']);

   if (!ObjectID.isValid(id)){
       return res.status(404).send();
   }

   if(_.isBoolean(body.completed) && body.completed) {
       body.completedAt = new Date().getTime();
   } else {
       body.completed = false;
       body.completedAt = null;
   }



   Todo.findByIdAndUpdate(id, {$set:body}, {new: true}).then((todo) =>{
       if(!todo){
           return res.status(404).send();
       }

       res.send({todo});
   }).catch((e) => {
       res.status(400).send();
   })

});

app.get('/user/:id', (req,res) => {
   console.log(req.params);

   var id = req.params.id;
    console.log(id);
    if(!ObjectID.isValid(id)){
        res
            .status(400)
            .send('Id not valid');
    }

    User.findById(
        id
    ).then((user) => {
        if(user){
        console.log(user);
        res.status(200).send({user})}
        else {
            res.status(404).send('User not exist');
        }
    }, (e) => {
        res.status(400).send(e);
    })
});

//post /users
app.post('/users' , (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
    var user = new User(body);

    user.save().then(() => {
        // res.send(user);
        return user.generateAuthToken();

    }).then((token) => {
        res.header('x-auth', token).send(user);
    }).catch((e) => {
        res.status(400).send(e);
    })
});

//middle ware for the Authentication


app.get('/users/me',authenticate , (req,res) => {

    // var token = req.header('x-auth');
    //
    // User.findByToken(token).then((user) => {
    //     if(!user){
    //
    //         return Promise.reject();
    //     }
    //
    //     res.send(user);
    //
    // }).catch((e) => {
    //     res.status(401).send();
    // });
    res.send(req.user);


});

//post /users/login {email , password}

app.post('/users/login',(req,res) => {
    var body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email,body.password).then((user) => {
        return user.generateAuthToken().then((token)=> {
            res.header('x-auth', token).send(user);
        })
        // res.send(user);

    }).catch((e) => {
        res.status(400).send();
    });

      // res.send(body);
});

app.delete('/users/me/token', authenticate , (req,res) => {
    req.user.removeToken(req.token).then(() => {
        res.status(200).send();
    }, () => {
        res.status(400).send();
    });
});


app.listen(port, () =>
   console.log(`Started on the port ${port}`)
);

module.exports = {app};